#!/bin/bash

set -euxo pipefail

# PCSK9 in GRCh37/hg19
REGION="1:55505221-55530525"

SAMPLE_ID="$(cat input.json | jq -r '.sample_id')"
VCF_URL="$(cat input.json | jq -r '.files["impute-vcf"].url')"
CSI_URL="$(cat input.json | jq -r '.files["impute-csi"].url')"

echo $SAMPLE_ID

bcftools view -r $REGION $VCF_URL##idx##$CSI_URL | bgzip -c | aws s3 cp - $OUTPUT_S3_PATH/$AWS_BATCH_JOB_ARRAY_INDEX/vcf/$SAMPLE_ID.vcf.gz
